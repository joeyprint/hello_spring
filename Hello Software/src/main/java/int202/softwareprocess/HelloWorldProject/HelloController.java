package int202.softwareprocess.HelloWorldProject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Controller
public class HelloController {

    @Autowired
    private HelloService helloService;

    @GetMapping("/{hello_id}")
    public String getTextHello (@PathVariable (name = "hello_id") long id, Model model) {
        Hello hello_data = helloService.getHelloById(id);
        model.addAttribute("hello", hello_data);
        return "index";
    }
}
