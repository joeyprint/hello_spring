package int202.softwareprocess.HelloWorldProject;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HelloWorldProjectApplicationTests {

	@Autowired
	private HelloService helloService;

	@Test
	public void contextLoads() {
	}

	@Test
	public void TestHelloText() {
		Hello hello_object = helloService.getHelloById(1);
		String expect = "Software Process I";
		String actual = hello_object.getText();
		assertEquals(expect, actual);
	}

}
